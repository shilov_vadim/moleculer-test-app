'use strict';

const {MoleculerError} = require('moleculer/index').Errors;

class BusinessError extends MoleculerError {
  constructor(msg, code, data) {
    super(msg || 'Business error.', 500, 'BUSINESS_ERROR', data);
  }
}

module.exports = {
  BusinessError
};
