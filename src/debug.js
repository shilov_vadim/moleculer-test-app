'use strict';

require('dotenv').config({path: `${__dirname}/../.env`});
const {ServiceBroker} = require('moleculer');

const broker = new ServiceBroker({
  nodeID: process.env.NODEID || `node_${process.pid}`,
  logger: process.env.LOGGER === 'true',
  logLevel: process.env.LOGLEVEL,
  transporter: process.env.TRANSPORTER,
  cacher: process.env.CACHER,
  hotReload: true
});

broker.loadServices('services', '*.service.js');

broker.start();
