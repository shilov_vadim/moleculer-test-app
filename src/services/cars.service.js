'use strict';

const {RequestRejectedError, ValidationError} = require('moleculer').Errors;
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
const moment = require('moment');

const {makes, colors, CURRENCY_REG_EXP} = require('../utils/constants');
const {BusinessError} = require('../utils/errors');

const SEED_CARS_LOCK_KEY = 'seed_cars';

module.exports = {
  name: 'cars',
  mixins: [DbService],
  adapter: new MongoDBAdapter(process.env.MONGODB, {useNewUrlParser: true}),
  collection: 'cars',
  settings: {
    idField: '_id',
    fields: ['_id', 'make', 'color', 'price'],
    pageSize: 100,
    maxPageSize: 1000,
    maxLimit: 1000,
    entityValidator: {
      make: {
        type: 'string',
        empty: false
      },
      color: {
        type: 'string',
        empty: false
      },
      price: {
        type: 'number',
        positive: true
      }
    }
  },
  hooks: {
    before: {
      get: 'checkCurrencyParam',
      find: 'checkCurrencyParam',
      list: 'checkCurrencyParam'
    },
    after: {
      get: 'convertPrices',
      find: 'convertPrices',
      list: 'convertPrices',
      seedCars: 'clearCache',
    }
  },
  actions: {
    get: {
      cache: {
        keys: ['currency']
      }
    },
    find: {
      cache: {
        keys: ['currency']
      }
    },
    list: {
      cache: {
        keys: ['currency']
      }
    },
    async seedCars(context) {
      const redis = context.broker.cacher.client;
      const isRun = await redis.get(SEED_CARS_LOCK_KEY);
      if (isRun) {
        throw new RequestRejectedError({
          nodeID: context.nodeID,
          action: context.action.rawName,
          cause: 'Action already run!'
        });
      }

      await redis.set(SEED_CARS_LOCK_KEY, 1, 'PX', 10000);

      const operations = makes.reduce((acc, make) => {
        const baseCost = this.getRandomInt(500000, 3000000);
        colors.forEach((color) => {
          const colorMarkup = this.getRandomInt(0, 50000);
          const entity = {
            make,
            color,
            price: baseCost + colorMarkup
          };
          acc.push({
            updateOne: {
              filter: {
                make: entity.make,
                color: entity.color
              },
              update: entity,
              upsert: true
            }
          });
        });
        return acc;
      }, []);

      const writeResult = await this.adapter.collection.bulkWrite(operations);
      this.logger.info('Seeding results:', {
        modified: writeResult.modifiedCount,
        upserted: writeResult.usertedCount
      });

      return redis.del(SEED_CARS_LOCK_KEY);
    }
  },
  events: {
    'cache.clean.rates'() {
      if (this.broker.cacher) {
        this.broker.cacher.clean('cars.*');
      }
    }
  },
  methods: {
    async checkCurrencyParam(context) {
      const {currency} = context.params;
      if (currency && !CURRENCY_REG_EXP.test(currency)) {
        throw new ValidationError('Wrong currency!', 'VALIDATION_ERROR', [{
          type: 'stringPattern',
          field: 'currency',
          message: 'The \'currency\' field fails to match the required pattern!'
        }]);
      }

      if (currency && currency !== 'RUB') {
        const response = await context.call('rates.find', {
          limit: 2,
          query: {
            currency,
            date: {
              $gte: moment()
                .subtract(1, 'days')
                .format()
            }
          }
        });

        const rate = Array.isArray(response) && response[0];
        if (!rate) {
          this.logger.error(`Cant't get an actual rate for the currency ${currency}!`);
          throw new BusinessError('Currency rate is not found!');
        }

        context.params.rate = rate;
      }
    },
    convertPrices(context, result) {
      if (context.params.currency && context.params.currency !== 'RUB') {
        const {rate: {rate, nominal}} = context.params;
        if (Array.isArray(result.rows) && result.rows.length) {
          result.rows.forEach((row) => {
            row.price = Math.round((row.price * 100) / (rate * nominal)) / 100;
          });
        }
      }
      return result;
    },
    getRandomInt(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
  },
  afterConnected() {
    this.logger.info('MongoDBAdapter is connected successfully.');
    return this.adapter.collection.createIndex({
      make: 1,
      color: 1
    }, {unique: true});
  }
};
