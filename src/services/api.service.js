'use strict';

const {MoleculerError} = require('moleculer').Errors;
const ApiGatewayService = require('moleculer-web/index');

module.exports = {
  port: process.env.PORT || 3000,
  mixins: [ApiGatewayService],
  settings: {
    routes: [{
      aliases: {
        'REST cars': 'cars',
        'REST rates': 'rates',
        'GET carsSeeding': 'cars.seedCars',
        'GET ratesUpdating': 'rates.updateRates'
      },
      mappingPolicy: 'restrict',
      bodyParsers: {
        json: true,
        urlencoded: {
          extended: true
        }
      }
    }],
    onError(req, res, err) {
      let code;
      let body;
      if (err instanceof MoleculerError) {
        code = err.code || 500;
        body = {
          code,
          message: err.message,
          type: err.type,
          data: err.data
        };
      } else if (err.name === 'MongoError' && err.code === 11000) {
        // E11000 duplicate key error collection: db.cars index: make_1_color_1 dup key: { : "Toyota", : "Red" }
        const matched = err.message.match(/dup key: { : "(.+)" }/);
        const values = matched ? matched[1].split('", : "') : null;

        code = 422;
        body = {
          code,
          message: 'Entity is not unique!',
          type: 'UNIQUE_ERROR',
          data: {values}
        };
      } else {
        code = 500;
        body = {
          code,
          message: 'Internal server error',
          type: 'INTERNAL_ERROR'
        };
      }

      res.setHeader('Content-type', 'application/json; charset=utf-8');
      res.writeHead(code);
      res.end(JSON.stringify(body, null, 2));
    }
  }
};
