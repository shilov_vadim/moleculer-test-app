'use strict';

const https = require('https');
const {RequestRejectedError} = require('moleculer').Errors;
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
const moment = require('moment');

const {CURRENCY_REG_EXP} = require('../utils/constants');
const {BusinessError} = require('../utils/errors');

const UPDATE_RATES_LOCK_KEY = 'update_rates';
const CBRF_DAILY_RATES_URL = 'https://www.cbr-xml-daily.ru/daily_json.js';

module.exports = {
  name: 'rates',
  mixins: [DbService],
  adapter: new MongoDBAdapter(process.env.MONGODB, {useNewUrlParser: true}),
  collection: 'rates',
  settings: {
    idField: '_id',
    fields: ['_id', 'date', 'currency', 'rate', 'nominal'],
    pageSize: 100,
    maxPageSize: 1000,
    maxLimit: 1000,
    entityValidator: {
      date: {
        type: 'date',
        convert: true
      },
      currency: {
        type: 'string',
        pattern: CURRENCY_REG_EXP
      },
      rate: {
        type: 'number',
        positive: true
      },
      nominal: {
        type: 'number',
        integer: true,
        min: 1
      }
    }
  },
  hooks: {
    after: {
      updateRates: 'clearCache',
    }
  },
  actions: {
    async updateRates(context) {
      const redis = context.broker.cacher.client;
      const isRun = await redis.get(UPDATE_RATES_LOCK_KEY);
      if (isRun) {
        throw new RequestRejectedError({
          nodeID: context.nodeID,
          action: context.action.rawName,
          cause: 'Action already run!'
        });
      }

      await redis.set(UPDATE_RATES_LOCK_KEY, 1, 'PX', 10000);

      const body = await new this.Promise((resolve, reject) => {
        https.get(CBRF_DAILY_RATES_URL, (resp) => {
          let data = '';

          resp.on('data', (chunk) => {
            data += chunk;
          });

          resp.on('end', () => {
            resolve(data);
          });
        })
          .on('error', (err) => {
            reject(err);
          });
      });

      const data = JSON.parse(body);
      if (!data.Date || !data.Valute) {
        throw new BusinessError('Unexpected response structure!');
      }

      const date = moment(data.Date, moment.defaultFormat);
      if (!date.isValid()) {
        this.logger.error(`Can't parse date: ${data.Date}!`);
        throw new BusinessError('Unexpected date format!');
      }

      const operations = Object.keys(data.Valute)
        .reduce((acc, currency) => {
          if (data.Valute[currency]) {
            const entity = this.prepareEntity(date.format(), data.Valute[currency]);
            acc.push({
              updateOne: {
                filter: {
                  date: entity.date,
                  currency: entity.currency
                },
                update: entity,
                upsert: true
              }
            });
          }
          return acc;
        }, []);
      if (!operations.length) {
        this.logger.info('Nothing to update!');
        return redis.del(UPDATE_RATES_LOCK_KEY);
      }

      const writeResult = await this.adapter.collection.bulkWrite(operations);
      this.logger.info('Updating results:', {
        modified: writeResult.modifiedCount,
        upserted: writeResult.usertedCount
      });

      return redis.del(UPDATE_RATES_LOCK_KEY);
    }
  },
  methods: {
    prepareEntity(date, item) {
      return {
        date,
        currency: item.CharCode,
        rate: item.Value,
        nominal: item.Nominal,
      };
    }
  },
  afterConnected() {
    this.logger.info('MongoDBAdapter is connected successfully.');
    this.adapter.collection.createIndex({
      date: -1,
      currency: 1,
    }, {unique: true});
  }
};
