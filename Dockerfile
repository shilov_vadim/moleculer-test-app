FROM node:10-alpine

RUN mkdir /app
WORKDIR /app

COPY package.json .

RUN npm i

COPY . .

CMD ["npm", "run", "docker:start"]
