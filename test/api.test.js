'use strict';

require('dotenv').config({path: 'test.env'});
const {MongoClient} = require('mongodb');
const {ServiceBroker} = require('moleculer');
const supertest = require('supertest');

const idRegExp = /^[\dA-Za-z]{24}$/;

describe('API tests', () => {
  const state = {
    address: `localhost:${process.env.PORT || 3000}`
  };

  beforeAll(async () => {
    state.mongo = await MongoClient.connect(process.env.MONGODB, {useNewUrlParser: true});

    await state.mongo.db()
      .dropDatabase();

    state.broker = new ServiceBroker({
      nodeID: 'test',
      logger: process.env.LOGGER === 'true',
      logLevel: process.env.LOGLEVEL,
      transporter: process.env.TRANSPORTER,
      cacher: {
        type: 'Redis',
        prefix: 'TEST',
        ttl: 10,
        redis: process.env.CACHER
      }
    });
    state.broker.loadServices('src/services', '*.service.js');
    await state.broker.start();
  });

  afterAll(async () => {
    await state.broker.stop();
    await state.mongo.db()
      .dropDatabase();
    await state.mongo.close(false);
  });

  test('should get an empty list of cars (GET /cars -> 200)', () => {
    return supertest(state.address)
      .get('/cars')
      .expect((response) => {
        expect(response)
          .toMatchObject({
            status: 200,
            body: {
              rows: [],
              total: 0,
              page: 1,
              pageSize: 100,
              totalPages: 0
            }
          });
      });
  });

  test('should get an empty list of rates (GET /rates -> 200)', () => {
    return supertest(state.address)
      .get('/rates')
      .expect((response) => {
        expect(response)
          .toMatchObject({
            status: 200,
            body: {
              rows: [],
              total: 0,
              page: 1,
              pageSize: 100,
              totalPages: 0
            }
          });
      });
  });

  test('should receive a validation error (POST /cars -> 422)', () => {
    return supertest(state.address)
      .post('/cars')
      .send({
        make: 'Toyota'
      })
      .expect((response) => {
        expect(response)
          .toMatchObject({
            status: 422,
            body: {
              code: 422,
              message: 'Entity validation error!',
              type: 'VALIDATION_ERROR',
              data: [
                {
                  type: 'required',
                  field: 'color',
                  message: 'The \'color\' field is required!'
                },
                {
                  type: 'required',
                  field: 'price',
                  message: 'The \'price\' field is required!'
                }
              ]
            }
          });
      });
  });

  test('should save a car (POST /cars -> 200)', () => {
    return supertest(state.address)
      .post('/cars')
      .send({
        make: 'Toyota',
        color: 'Red',
        price: 777777
      })
      .expect((response) => {
        expect(response)
          .toMatchObject({
            status: 200,
            body: {
              _id: expect.stringMatching(idRegExp),
              color: 'Red',
              make: 'Toyota',
              price: 777777
            }
          });
        state.savedCarId = response.body._id;
      });
  });

  test('should receive an unique error (POST /cars -> 422)', () => {
    return supertest(state.address)
      .post('/cars')
      .send({
        make: 'Toyota',
        color: 'Red',
        price: 777777
      })
      .expect((response) => {
        expect(response)
          .toMatchObject({
            status: 422,
            body: {
              code: 422,
              type: 'UNIQUE_ERROR',
              message: 'Entity is not unique!',
              data: {
                values: [
                  'Toyota',
                  'Red'
                ]
              }
            }
          });
      });
  });

  test('should update a car (PUT /cars/:id -> 200)', () => {
    return supertest(state.address)
      .put(`/cars/${state.savedCarId}`)
      .send({
        color: 'Green'
      })
      .expect((response) => {
        expect(response)
          .toMatchObject({
            status: 200,
            body: {
              _id: state.savedCarId,
              make: 'Toyota',
              color: 'Green',
              price: 777777
            }
          });
      });
  });

  test('should get a car by id (GET /cars/:id -> 200)', () => {
    return supertest(state.address)
      .get(`/cars/${state.savedCarId}`)
      .expect((response) => {
        expect(response)
          .toMatchObject({
            status: 200,
            body: {
              _id: state.savedCarId,
              make: 'Toyota',
              color: 'Green',
              price: 777777
            }
          });
      });
  });

  test('should receive an error (GET /cars/:id?currency=EUR -> 500)', () => {
    return supertest(state.address)
      .get(`/cars/${state.savedCarId}?currency=EUR`)
      .expect((response) => {
        expect(response)
          .toMatchObject({
            status: 500,
            body: {
              code: 500,
              message: 'Currency rate is not found!',
              type: 'BUSINESS_ERROR'
            }
          });
      });
  });

  test('should save a rate (POST /rates -> 200)', () => {
    state.savedRateDate = new Date();
    return supertest(state.address)
      .post('/rates')
      .send({
        date: state.savedRateDate,
        currency: 'EUR',
        rate: 70,
        nominal: 1
      })
      .expect((response) => {
        expect(response)
          .toMatchObject({
            status: 200,
            body: {
              _id: expect.stringMatching(idRegExp),
              date: state.savedRateDate.toISOString(),
              currency: 'EUR',
              rate: 70,
              nominal: 1
            }
          });
        state.savedRateId = response.body._id;
      });
  });

  test('should receive a unique error (POST /rates -> 422)', () => {
    return supertest(state.address)
      .post('/rates')
      .send({
        date: state.savedRateDate,
        currency: 'EUR',
        rate: 70,
        nominal: 1
      })
      .expect((response) => {
        expect(response)
          .toMatchObject({
            status: 422,
            body: {
              code: 422,
              type: 'UNIQUE_ERROR',
              message: 'Entity is not unique!',
              data: {
                values: [
                  state.savedRateDate.toISOString(),
                  'EUR'
                ]
              }
            }
          });
      });
  });

  test('should update a rate (PUT /rates/:id -> 200)', () => {
    return supertest(state.address)
      .put(`/rates/${state.savedRateId}`)
      .send({
        rate: 77
      })
      .expect((response) => {
        expect(response)
          .toMatchObject({
            status: 200,
            body: {
              _id: state.savedRateId,
              date: state.savedRateDate.toISOString(),
              currency: 'EUR',
              rate: 77,
              nominal: 1
            }
          });
      });
  });

  test('should get a car by id (GET /cars/:id?currency=EUR -> 200)', () => {
    return supertest(state.address)
      .get(`/cars/${state.savedCarId}?currency=EUR`)
      .expect((response) => {
        expect(response)
          .toMatchObject({
            status: 200,
            body: {
              _id: state.savedCarId,
              make: 'Toyota',
              color: 'Green',
              price: 777777
            }
          });
      });
  });

  test('should delete a car by id (DELETE /cars/:id -> 200)', () => {
    return supertest(state.address)
      .delete(`/cars/${state.savedCarId}`)
      .expect((response) => {
        expect(response)
          .toMatchObject({
            status: 200,
            body: {
              _id: state.savedCarId,
              make: 'Toyota',
              color: 'Green',
              price: 777777
            }
          });
      });
  });

  test('should seed cars (GET /carsSeeding -> 200)', () => {
    return supertest(state.address)
      .get('/carsSeeding')
      .expect((response) => {
        expect(response)
          .toMatchObject({
            status: 200,
            body: {}
          });
      });
  });

  test('should get a list of cars (GET /cars/?pageSize=5 -> 200)', () => {
    return supertest(state.address)
      .get('/cars/?pageSize=5')
      .expect((response) => {
        expect(response)
          .toMatchObject({
            status: 200,
            body: {
              rows: [
                {
                  _id: expect.stringMatching(idRegExp),
                  make: 'Aston Martin',
                  color: 'Aluminum',
                  price: expect.any(Number)
                },
                {
                  _id: expect.stringMatching(idRegExp),
                  make: 'Aston Martin',
                  color: 'Beige',
                  price: expect.any(Number)
                },
                {
                  _id: expect.stringMatching(idRegExp),
                  make: 'Aston Martin',
                  color: 'Black',
                  price: expect.any(Number)
                },
                {
                  _id: expect.stringMatching(idRegExp),
                  make: 'Aston Martin',
                  color: 'Blue',
                  price: expect.any(Number)
                },
                {
                  _id: expect.stringMatching(idRegExp),
                  make: 'Aston Martin',
                  color: 'Brown',
                  price: expect.any(Number)
                }
              ],
              total: 4108,
              page: 1,
              pageSize: 5,
              totalPages: 822
            }
          });
      });
  });

  test('should get a list of cars (GET /cars/?page=2&pageSize=5 -> 200)', () => {
    return supertest(state.address)
      .get('/cars/?page=2&pageSize=5')
      .expect((response) => {
        expect(response)
          .toMatchObject({
            status: 200,
            body: {
              rows: [
                {
                  _id: expect.stringMatching(idRegExp),
                  make: 'Aston Martin',
                  color: 'Bronze',
                  price: expect.any(Number)
                },
                {
                  _id: expect.stringMatching(idRegExp),
                  make: 'Aston Martin',
                  color: 'Claret',
                  price: expect.any(Number)
                },
                {
                  _id: expect.stringMatching(idRegExp),
                  make: 'Aston Martin',
                  color: 'Copper',
                  price: expect.any(Number)
                },
                {
                  _id: expect.stringMatching(idRegExp),
                  make: 'Aston Martin',
                  color: 'Cream',
                  price: expect.any(Number)
                },
                {
                  _id: expect.stringMatching(idRegExp),
                  make: 'Aston Martin',
                  color: 'Gold',
                  price: expect.any(Number)
                }
              ],
              total: 4108,
              page: 2,
              pageSize: 5,
              totalPages: 822
            }
          });
        state.secondPageOfCars = response.body.rows;
      });
  });


  test('should get a list of cars (GET /cars/?page=2&pageSize=5&currency=EUR -> 200)', () => {
    return supertest(state.address)
      .get('/cars/?page=2&pageSize=5&currency=EUR')
      .expect((response) => {
        expect(response)
          .toMatchObject({
            status: 200,
            body: {
              rows: [
                {
                  _id: expect.stringMatching(idRegExp),
                  make: 'Aston Martin',
                  color: 'Bronze',
                  price: expect.any(Number)
                },
                {
                  _id: expect.stringMatching(idRegExp),
                  make: 'Aston Martin',
                  color: 'Claret',
                  price: expect.any(Number)
                },
                {
                  _id: expect.stringMatching(idRegExp),
                  make: 'Aston Martin',
                  color: 'Copper',
                  price: expect.any(Number)
                },
                {
                  _id: expect.stringMatching(idRegExp),
                  make: 'Aston Martin',
                  color: 'Cream',
                  price: expect.any(Number)
                },
                {
                  _id: expect.stringMatching(idRegExp),
                  make: 'Aston Martin',
                  color: 'Gold',
                  price: expect.any(Number)
                }
              ],
              total: 4108,
              page: 2,
              pageSize: 5,
              totalPages: 822
            }
          });
        response.body.rows.forEach((car, i) => {
          expect(car.price)
            .toBe(Math.round(state.secondPageOfCars[i].price / 77 * 100) / 100);
        });
      });
  });

  test('should delete a rate by id (DELETE /rates/:id -> 200)', () => {
    return supertest(state.address)
      .delete(`/rates/${state.savedRateId}`)
      .expect((response) => {
        expect(response)
          .toMatchObject({
            status: 200,
            body: {
              _id: state.savedRateId,
              date: state.savedRateDate.toISOString(),
              currency: 'EUR',
              rate: 77,
              nominal: 1
            }
          });
      });
  });
});
