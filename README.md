# moleculer-test-app

### Запуск:

#### Docker:

Env файл: `docker-compose.env`

Команад: `npm i && npm run docker:build && docker-compose up -d`

#### moleculer-runner:

Env файл: `.env`

Команда: `docker-compose up -d mongo redis nats && npm start`

#### Node:

Env файл: `.env`

Команда: `docker-compose up -d mongo redis nats && node src/debug.js`

### Запуск тестов

Env файл: `test.env`

Команда: `docker-compose up -d mongo redis nats && npm test`

